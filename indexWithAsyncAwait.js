const express = require('express')
const app = express()
const port = process.env.PORT || 3000
const model = require('./models')
const todo = model.ToDo
const user = model.User

app.use(express.json())

/* Using async-await */
app.get('/todo', async (req, res)  => {
    try{
        const dataTodo = await todo.findAll()
        res.status(200).json({
            status: "true",
            message: "List Todo",
            data: dataTodo
        })
    }catch(err){
        res.status(422).json({
            status: "false",
            message: err.message
        })
    }
})

app.get('/user', async (req, res)  => {
    try{
        const dataUser = await user.findAll()
        res.status(200).json({
            status: "true",
            message: "List User",
            data: dataUser
        })
    }catch(err){
        res.status(422).json({
            status: "false",
            message: err.message
        })
    }
})

app.post('/todo', async (req, res)  => {
    try{
        const userVal = req.body.user_id
        const {count} = await user.findAndCountAll({
            where:{
                id : userVal
            }
        })
        if(count != 1){
            res.status(200).json({
                status: "false",
                message: `user_id ${userVal} not found !!`,
            })
        }else{
            try{
                todo.create(req.body)
                res.status(200).json({
                    status: "true",
                    message: `user_id ${userVal} ready, todo created !!`,
                    data: req.body
                })
            }catch(err){
                res.status(422).json({
                    status: "false",
                    message: err.message
                })
            }
        }
    }catch(err){
        res.status(422).json({
            status: "false",
            message: err.message
        })
    }
})

app.post('/user', async (req, res)  => {
    try{
        const emailVal = req.body.email
        const {count} = await user.findAndCountAll({
            where:{
                email:emailVal
            }
        })
        if(count == 1){
            res.status(200).json({
                status: "false",
                message: "Email Already",
            })
        }else{
            user.create(req.body)
            res.status(200).json({
                status: "true",
                message: "User Created !!",
                data: req.body
            })
        }
    }catch(err){
        res.status(422).json({
            status: "false",
            message: err.message
        })
    }
})

app.put('/todo/:id', async (req, res)  => {
    try{
        const dataId = await todo.findByPk(req.params.id)
        if(dataId){
            const dataUpdate = await todo.update ({
                name: req.body.name,
                description: req.body.description,
                due_at: req.body.due_at,
                user_id: req.body.user_id
                },{
                    where : {
                        id: req.params.id
                    }
                })
            
            res.status(200).json({
                status: "true",
                message: `ToDo Update id:${req.params.id}`,
                data: dataUpdate
            })
        }else{
            res.status(422).json({
                status: "false",
                message: "id not found !!"
            })
        }
    }catch(err){
        res.status(422).json({
            status: "false",
            message: err.message
        })
    }
})

app.put('/user/:id', async (req, res)  => {
    try{
        const userId = await user.findByPk(req.params.id)
        if(userId){
            const dataUserUpdate = await user.update ({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
                },{
                    where : {
                        id: req.params.id
                    }
                })
            
            res.status(200).json({
                status: "true",
                message: `User Update id:${req.params.id}`,
                data: dataUserUpdate
            })
        }else{
            res.status(422).json({
                status: "false",
                message: "id not found !!"
            })
        }
    }catch(err){
        res.status(422).json({
            status: "false",
            message: err.message
        })
    }
})

app.delete('/todo/:id', async (req, res)  => {
    try{
        const searchTodoId = await todo.findByPk(req.params.id)
        if(searchTodoId){
            const deleteTodoId = await todo.destroy({
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                status: "true",
                message: `row id ${req.params.id}, successfuly deleted`
            })
        }else{
            res.status(422).json({
                status: "false",
                message: "id not found !!"
            })
        }  
    }catch(err){
        res.status(422).json({
            status: "false",
            message: err.message
        })
    }
})

app.delete('/user/:id', async (req, res)  => {
    try{
        const searchUserId = await user.findByPk(req.params.id)
        if(searchUserId){
            const deleteUserId = await user.destroy({
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                status: "true",
                message: `row id ${req.params.id}, successfuly deleted`
            })
        }else{
            res.status(422).json({
                status: "false",
                message: "id not found !!"
            })
        }  
    }catch(err){
        res.status(422).json({
            status: "false",
            message: err.message
        })
    }
})

app.listen(port, () => {
    console.log(`Listening port ${port}!!`)
})