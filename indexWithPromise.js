const express = require('express')
const app = express()
const port = process.env.PORT || 3000
const model = require('./models')
const todo = model.ToDo
const user = model.User

app.use(express.json())

/* Using Promise */
app.get('/todo', (req, res)  => {
    todo.findAll()
    .then((todo) => {
        res.status(200).json({
            status: "true",
            message: "List Todo",
            data: todo
        })
    })
    .catch((todo) => {
        res.status(422).json({
            status: "false",
            message: "List Todo",
        })
    })
})

app.get('/user', (req, res)  => {
    user.findAll()
    .then((user) => {
        res.status(200).json({
            status: "true",
            message: "List User",
            data: user
        })
    })
    .catch((user) => {
        res.status(422).json({
            status: "false",
            message: err.message
        })
    })
})

app.post('/todo', (req, res)  => {
    const userVal = req.body.user_id
    user.findAndCountAll({
        where:{
            id : userVal
        }
    })
    .then((user) => {
        if(user.count != 1){
            res.status(200).json({
                status: "false",
                message: `user_id ${userVal} not found !!`,
            })
        }else{
            todo.create(req.body)
            try{
                res.status(200).json({
                    status: "true",
                    message: `user_id ${userVal} ready, todo created !!`,
                    data: req.body
                })
            }catch(err){
                res.status(422).json({
                    status: "false",
                    message: err.message
                })
            }
        }
    })
    .catch((err) => {
        res.status(422).json({
            status: "false",
            message: err.message
        })
    })
})

app.post('/user', (req, res)  => {
    const emailVal = req.body.email
    user.findOrCreate({
        where:{
            email:emailVal
        },
        defaults:{
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        }
    })
    .then((user) => {
        if(user[1]){
            res.status(200).json({
                status: "true",
                message: "User Created !!",
                data: user[0]
            })
        }else{
            res.status(200).json({
                status: "false",
                message: "Email Already",
            })
        }
    })
    .catch((err) => {
        res.status(422).json({
            status: "false",
            message: err.message
        })
    })
})

app.put('/todo/:id', (req, res)  => {
    todo.findByPk(req.params.id)
    .then((todo) => {
        todo.update ({
            name: req.body.name,
            description: req.body.description,
            due_at: req.body.due_at,
            user_id: req.body.user_id
        },{
            where : req.params.id
        }).then(() => {
            res.status(200).json({
                status: 200,
                message: `ToDo Update id:${req.params.id}`,
                data: todo
            })
        })
    })
    .catch((todo) => {
        res.status(422).json({
            status: "false",
            message: "id not ready"
        })
    })
})

app.put('/user/:id', (req, res)  => {
    user.findByPk(req.params.id)
    .then((user) => {
        user.update ({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        },{
            where : req.params.id
        })
        .then(() => {
            res.status(200).json({
                status: 200,
                message: `User Update id:${req.params.id}`,
                data: user
            })
        })
    })
    .catch((user) => {
        res.status(422).json({
            status: "false",
            message: "id not ready"
        })
    })
})

app.delete('/todo/:id', (req, res)  => {
    const idVal = req.params.id
    todo.findByPk(req.params.id)
    .then((todo) => {
        todo.destroy({
            where: {
                id: req.params.id
            }
        })
        res.status(200).json({
            status: "trus",
            message: `row id ${req.params.id}, successfuly deleted`
        })
    })
    .catch((todo) => {
        res.status(422).json({
            status: "false",
            message: "id not ready"
        })
    })
})

app.delete('/user/:id', (req, res)  => {
    const idVal = req.params.id
    user.findByPk(req.params.id)
    .then((user) => {
        user.destroy({
            where: {
                id: req.params.id
            }
        })
        res.status(200).json({
            status: "trus",
            message: `row id ${req.params.id}, successfuly deleted`
        })
    })
    .catch((user) => {
        res.status(422).json({
            status: "false",
            message: "id not ready"
        })
    })
})

app.listen(port, () => {
    console.log(`Listening port ${port}!!`)
})